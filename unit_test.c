#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include "merge_sort.h"
#include "utils_integer.h"

int main() {
    size_t N = 10000;
    size_t size = sizeof(int);
    printf("Starting unit tests\n");
    int array1[8] = {0, 1, 2, 3, 3, 5, 6, 7};
    int array2[8] = {0, 1, 2, 6, 5, 7, 8, 9};
    printf("Unit tests for CompareInt() - ");
    assert(CompareInt((void *)&array1[2], (void *)&array1[1]) == 1);
    assert(CompareInt((void *)&array1[3], (void *)&array1[4]) == 0);
    assert(CompareInt((void *)&array1[5], (void *)&array2[6]) == -3);
    printf("OK\n");
    printf("Unit tests for isSorted() - ");
    assert(isSorted((void *)array1, 8, size, CompareInt));
    assert(!isSorted((void *)array2, 8, size, CompareInt));
    printf("OK\n");
    printf("Unit tests for BinarySearch() - ");
    int key = 4;
    assert(BinarySearch((void *)array1, 8, size, (void *)&key,
                        CompareInt) == 5);
    key = 2;
    assert(BinarySearch((void *)array1, 8, size, (void *)&key,
                        CompareInt) == 2);
    key = 6;
    assert(BinarySearch((void *)array1, 8, size, (void *)&key,
                        CompareInt) == 6);
    key = 3;
    assert(BinarySearch((void *)array1, 8, size, (void *)&key,
                        CompareInt) == 3);
    key = 10;
    assert(BinarySearch((void *)array1, 8, size, (void *)&key,
                        CompareInt) == 8);
    key = -1;
    assert(BinarySearch((void *)array1, 8, size, (void *)&key,
                        CompareInt) == 0);
    printf("OK\n");
    int array3[5] = {0, 2, 3, 4, 8};
    int resulting_array[13];
    int resulting_array2[13];
    int resulting_array_sample[13] = {0, 0, 1, 2, 2, 3, 3, 3, 4, 5, 6, 7, 8};
    printf("Unit tests for UsualMerge() - ");
    UsualMerge((void *)array1, 8, (void *)array3, 5, (void *)resulting_array2,
          size, CompareInt);
    assert(memcmp((void *)resulting_array2, (void *)resulting_array_sample,
                  13 * size) == 0);
    for (int i = 0; i < 100; ++i) {
        int *first_array = malloc(N * size);
        int *second_array = malloc(N * size);
        int *array_for_qsort = malloc(2 * N * size);
        int *array_for_merge = malloc(2 * N * size);
        InitializeByRandomNumbers(first_array, N);
        qsort(first_array, N, size, CompareInt);
        InitializeByRandomNumbers(second_array, N);
        qsort(second_array, N, size, CompareInt);
        memcpy(array_for_qsort, first_array, N * size);
        memcpy((void *)((char *)(array_for_qsort) + N * size), second_array,
             N * size);
        qsort(array_for_qsort, 2 * N, size, CompareInt);
        UsualMerge(first_array, N, second_array, N, array_for_merge, size,
                    CompareInt);
        assert(isSorted(array_for_merge, 2 * N, size, CompareInt));
        assert(isSorted(array_for_qsort, 2 * N, size, CompareInt));
        assert(memcmp(array_for_qsort, array_for_merge, 2 * N * size) == 0);
        free(first_array);
        free(second_array);
        free(array_for_qsort);
        free(array_for_merge);
    }
    printf("OK\n");
    printf("Unit tests for ParallelMerge() - ");
    ParallelMerge((void *)array1, 8, (void *)array3, 5, (void *)resulting_array,
                  size, 1, CompareInt);
    assert(memcmp((void *)resulting_array, (void *)resulting_array_sample,
                  13 * size) == 0);
    for (int i = 0; i < 100; ++i) {
        int *first_array = malloc(N * size);
        int *second_array = malloc(N * size);
        int *array_for_qsort = malloc(2 * N * size);
        int *array_for_merge = malloc(2 * N * size);
        InitializeByRandomNumbers(first_array, N);
        qsort(first_array, N, size, CompareInt);
        InitializeByRandomNumbers(second_array, N);
        qsort(second_array, N, size, CompareInt);
        memcpy(array_for_qsort, first_array, N * size);
        memcpy((void *)((char *)(array_for_qsort) + N * size), second_array,
               N * size);
        qsort(array_for_qsort, 2 * N, size, CompareInt);
        ParallelMerge(first_array, N, second_array, N, array_for_merge, size,
                      i, CompareInt);
        assert(isSorted(array_for_merge, 2 * N, size, CompareInt));
        assert(memcmp(array_for_qsort, array_for_merge, 2 * N * size) == 0);
        free(first_array);
        free(second_array);
        free(array_for_qsort);
        free(array_for_merge);
    }
    printf("OK\n");
    printf("Unit tests for MergeSort() - ");
    int array2_sample[8] = {0, 1, 2, 5, 6, 7, 8, 9};
    MergeSort((void *)array2, 8, size, 1, CompareInt);
    assert(memcmp((void *)array2, (void *)array2_sample, 8 * size) == 0);
    for (int i = 0; i < 10; ++i) {
        int *array_for_qsort = malloc(N * size);
        int *array_for_merge_sort = malloc(N * size);
        InitializeByRandomNumbers(array_for_qsort, N);
        memcpy(array_for_merge_sort, array_for_qsort, N * size);
        qsort(array_for_qsort, N, size, CompareInt);
        MergeSort(array_for_merge_sort, N, size, (size_t)(i + 1), CompareInt);
        assert(isSorted(array_for_merge_sort, N, size, CompareInt));
        assert(memcmp(array_for_qsort, array_for_merge_sort, N * size) == 0);
        free(array_for_qsort);
        free(array_for_merge_sort);
    }
    printf("OK\n");
    printf("All tests are passesd successfully!\n");
    return 0;
}
