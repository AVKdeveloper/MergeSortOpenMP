all: single_thread multi_threads

single_thread:
	gcc -fopenmp single_thread.c -o single_thread
multi_threads:
	gcc -fopenmp multi_threads.c -o multi_threads
run_test: clean_test test
	./test
test:
	gcc unit_test.c -o test
clean: clean_test
	rm single_thread multi_threads data.txt
clean_test:
	if [ -f test ]; then rm test; fi
