#ifndef MERGE_SORT_H
#define MERGE_SORT_H

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

bool isSorted(const void *array, size_t length, size_t size,
              int (*compare)(const void *, const void *)) {
    for (int i = 0; i < length - 1; ++i) {
        if (compare((void *)((char *)(array) + i * size),
                    (void *)((char *)(array) + (i + 1) * size)) > 0) {
            return false;
        }
    }
    return true;
}

size_t BinarySearch(const void *array, size_t length, size_t size, void *key,
                 int (*compare)(const void *, const void *)) {
    // The function returns the index of first element which is >= key
    int left = -1;
    int right = length;
    while (left < right - 1) {
        int middle = (left + right) / 2;
        if (compare((void *)((char *)(array) + middle * size), key) >= 0) {
            right = middle;
        } else {
            left = middle;
        }
    }
    return right;
}

void UsualMerge(void *array1, size_t length1, void *array2, size_t length2,
           void *resulting_array, size_t size,
           int (*compare)(const void *, const void *)) {
    // Merge two sorted arrays to resulting array, which has the right size
    size_t idx1 = 0;
    size_t idx2 = 0;
    while (idx1 < length1 && idx2 < length2) {
        if (compare((void *)((char *)(array1) + idx1 * size),
                    (void *)((char *)(array2) + idx2 * size)) <= 0) {
            memcpy((void *)((char *)(resulting_array) + (idx1 + idx2) * size),
                   (void *)((char *)(array1) + idx1 * size), size);
            idx1++;
        } else {
            memcpy((void *)((char *)(resulting_array) + (idx1 + idx2) * size),
                   (void *)((char *)(array2) + idx2 * size), size);
            idx2++;
        }
    }
    memcpy((void *)((char *)(resulting_array) + (idx1 + idx2) * size),
           (void *)((char *)(array1) + idx1 * size), (length1 - idx1) * size);
    memcpy((void *)((char *)(resulting_array) + (idx1 + idx2) * size),
           (void *)((char *)(array2) + idx2 * size), (length2 - idx2) * size);
}

void ParallelMerge(void *array1, size_t length1, void *array2, size_t length2,
           void *resulting_array, size_t size, size_t max_chunk_length,
           int (*compare)(const void *, const void *)) {
    // Merge two sorted arrays to resulting array, which has the right size
    if (length1 + length2 <= max_chunk_length) {
        UsualMerge(array1, length1, array2, length2, resulting_array, size,
                   compare);
        return;
    }
    if (length1 == 0) {
        memcpy(resulting_array, array2, length2 * size);
        return;
    }
    size_t mid1 = length1 / 2;
    size_t mid2 = BinarySearch(array2, length2, size,
        (void *)((char *)(array1) + mid1 * size), compare);
    size_t mid3 = mid1 + mid2;
    memcpy((void *)((char *)(resulting_array) + mid3 * size),
           (void *)((char *)(array1) + mid1 * size), size);
    #pragma omp task
    ParallelMerge(array1, mid1, array2, mid2, resulting_array, size,
                  max_chunk_length, compare);
    #pragma omp task
    ParallelMerge((void *)((char *)(array1) + (mid1 + 1) * size),
        length1 - (mid1 + 1), (void *)((char *)(array2) + mid2 * size),
        length2 - mid2,
        (void *)((char *)(resulting_array) + (mid3 + 1) * size), size,
        max_chunk_length, compare);
    #pragma omp taskwait
}

void MergeSortWithArray(void *array, size_t length, size_t size,
               size_t max_chunk_length,
               int (*compare)(const void *, const void *), void *tmp_array) {
    if (length <= max_chunk_length) {
        qsort(array, length, size, compare);
    } else {
        size_t new_length = length / 2;
        #pragma omp task
        MergeSortWithArray(array, new_length, size, max_chunk_length,
            compare, tmp_array);
        #pragma omp task
        MergeSortWithArray((void *)((char *)(array) + new_length * size),
            length - new_length, size, max_chunk_length, compare,
            (void *)((char *)(tmp_array) + new_length * size));
        #pragma omp taskwait
        ParallelMerge(array, new_length,
            (void *)((char *)(array) + new_length * size),
            length - new_length, tmp_array, size, max_chunk_length, compare);
        memcpy(array, tmp_array, length * size);
    }
}

void MergeSort(void *array, size_t length, size_t size, size_t max_chunk_length,
               int (*compare)(const void *, const void *)) {
    void *tmp_array = malloc(size * length);
    #pragma omp parallel
    {
        #pragma omp single
        {
            MergeSortWithArray(array, length, size, max_chunk_length, compare,
                tmp_array);
        }
    }
    free(tmp_array);
}

 #endif  // MERGE_SORT_H_
